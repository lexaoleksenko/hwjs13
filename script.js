let wrapp = document.querySelector('.images-wrapper')
let offset = 0;
let StopBtn = document.querySelector('.btn-stop')
let StartBtn = document.querySelector('.btn-start')
let isPaused = false;

StopBtn.addEventListener('click', function(){
    isPaused = true;
})

StartBtn.addEventListener('click', function(){
    isPaused = false;
})

function nextSlide(){
    if (offset >= 1350){
        offset = 0;
    } else if (!isPaused) {
        offset += 450;
    }   
    wrapp.style.marginLeft = -offset + 'px';
}

setInterval(nextSlide,3000)